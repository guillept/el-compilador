/*
  Wyvern compiler - Program parser.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/


using System;
using System.Collections.Generic;

namespace Wyvern {

    class Parser {      

        static readonly ISet<TokenCategory> defList =
            new HashSet<TokenCategory>() {
                TokenCategory.VAR,
                TokenCategory.IDENTIFIER
            };
        
        static readonly ISet<TokenCategory> opComList =
            new HashSet<TokenCategory>() {
                TokenCategory.COMPARISON,
                TokenCategory.DIST_COMPARISON
            };

        static readonly ISet<TokenCategory> opRelList =
            new HashSet<TokenCategory>() {
                TokenCategory.LESS,
                TokenCategory.LESS_EQUAL,
                TokenCategory.GREATER,
                TokenCategory.GREATER_EQUAL
            };

        static readonly ISet<TokenCategory> opAddList =
            new HashSet<TokenCategory>() {
                TokenCategory.PLUS,
                TokenCategory.NEG
            };

        static readonly ISet<TokenCategory> opMulList =
            new HashSet<TokenCategory>() {
                TokenCategory.MUL,
                TokenCategory.DIV,
                TokenCategory.REMINDER
            };

        static readonly ISet<TokenCategory> opUnaryList =
            new HashSet<TokenCategory>() {
                TokenCategory.PLUS,
                TokenCategory.NEG,
                TokenCategory.NOT
            };

        static readonly ISet<TokenCategory> opLitList =
            new HashSet<TokenCategory>() {
                TokenCategory.BOOL_LIT,
                TokenCategory.INT_LIT,
                TokenCategory.CHAR_LIT,
                TokenCategory.STRING_LIT,
                TokenCategory.PARENTHESIS_CLOSE,
                TokenCategory.IDENTIFIER
            };

        static readonly ISet<TokenCategory> exprList =
            new HashSet<TokenCategory>() {
                TokenCategory.IDENTIFIER,
                TokenCategory.IF,
                TokenCategory.WHILE,
                TokenCategory.BREAK,
                TokenCategory.RETURN,
                TokenCategory.SEMICOLON
            };

        static readonly ISet<TokenCategory> statementList =
            new HashSet<TokenCategory>() {
                TokenCategory.ASSIGN,
                TokenCategory.PLUS_PLUS,
                TokenCategory.NEG_NEG,
                TokenCategory.PARENTHESIS_OPEN,
            };


        IEnumerator<Token> tokenStream;

        public Parser(IEnumerator<Token> tokenStream) {
            this.tokenStream = tokenStream;
            this.tokenStream.MoveNext();
        }

        public TokenCategory CurrentToken {
            get { return tokenStream.Current.Category; }
        }

        public Token Expect(TokenCategory category) {
            if (CurrentToken == category) {
                Token current = tokenStream.Current;
                tokenStream.MoveNext();
                return current;
            } else {
                throw new SyntaxError(category, tokenStream.Current);                
            }
        }

        
        public Node Program() {

            var program = new Program();
            var varList = new VarList();
            var funDefList = new FunDefList();

            while (defList.Contains(CurrentToken)) {
                if( CurrentToken == TokenCategory.VAR )
                    varList.Add(VarDef());
                else
                    funDefList.Add(FunDef());
            }

            program.Add(varList);
            program.Add(funDefList);
            Expect(TokenCategory.EOF);

            return program;
        }

        public Node VarDef() {

            var varDef = new VarDef{
                AnchorToken = Expect(TokenCategory.VAR)
            };

            VarList(ref varDef);

            Expect(TokenCategory.SEMICOLON);
            return varDef;
        }

        public void VarList(ref VarDef varDef) {
            var IdList = new IdList(){
                AnchorToken = Expect(TokenCategory.IDENTIFIER)
            };

            varDef.Add(IdList);

            while(CurrentToken == TokenCategory.COMMA) {

                Expect(TokenCategory.COMMA);
                var idListCont = new IdList(){
                    AnchorToken = Expect(TokenCategory.IDENTIFIER)
                };

                varDef.Add(idListCont);

            }
        }

        public Node FunDef() {
            var funDef = new FunDef() {
                AnchorToken = Expect(TokenCategory.IDENTIFIER)
            };

            Expect(TokenCategory.PARENTHESIS_OPEN);
            var paramList = new ParamList();
            ParamList(ref paramList);
            funDef.Add(paramList);
            Expect(TokenCategory.PARENTHESIS_CLOSE);

            Expect(TokenCategory.CURLY_OPEN);
            var varList = new VarList();
            while (CurrentToken == TokenCategory.VAR) {
                varList.Add(VarDef());
            }
            funDef.Add(varList);
            funDef.Add(StmtList());
            Expect(TokenCategory.CURLY_CLOSE);

            return funDef;
        }

        public void ParamList(ref ParamList paramList) {
            while(CurrentToken != TokenCategory.PARENTHESIS_CLOSE) {

                var idList = new IdList(){
                    AnchorToken = Expect(TokenCategory.IDENTIFIER)
                };

                paramList.Add(idList);
               
                while(CurrentToken == TokenCategory.COMMA) {

                    Expect(TokenCategory.COMMA);
                    var idListCont = new IdList(){
                        AnchorToken = Expect(TokenCategory.IDENTIFIER)
                    };

                    paramList.Add(idListCont);

                }
            }
        }
 
        public Node StmtList() {
            var stmtList = new StmtList();
            while(exprList.Contains(CurrentToken)) {
                Stmt(ref stmtList);
            }
            return stmtList;
        }

         public void Stmt(ref StmtList stmtList) {
            switch (CurrentToken) {
                case TokenCategory.IDENTIFIER:
                    var idToken = Expect(TokenCategory.IDENTIFIER);
                    if(CurrentToken == TokenCategory.ASSIGN) {
                        stmtList.Add(StmtAssign(idToken));
                    } else if(CurrentToken == TokenCategory.PLUS_PLUS) {
                        stmtList.Add(StmtIncr(idToken));
                    } else if (CurrentToken == TokenCategory.NEG_NEG){ 
                        stmtList.Add(StmtDecr(idToken));
                    } else if (CurrentToken == TokenCategory.PARENTHESIS_OPEN){
                        stmtList.Add(StmtFunCall(idToken));
                        StmtEmpty();
                    } else { 
                        throw new SyntaxError(statementList, tokenStream.Current);
                    }
                    break;
            
                case TokenCategory.WHILE:
                    
                    stmtList.Add(StmtWhile());
                    break;
                case TokenCategory.IF:
                    stmtList.Add(StmtIf());
                    break;
                case TokenCategory.RETURN:
                    stmtList.Add(StmtReturn());
                    break;
                case TokenCategory.BREAK:
                    stmtList.Add(StmtBreak());
                    break;
                case TokenCategory.SEMICOLON:
                    StmtEmpty();
                    break;
                default:
                    throw new SyntaxError(exprList, tokenStream.Current);
            }
        }

        public Node StmtAssign(Token idToken) {
            Expect(TokenCategory.ASSIGN);
            var stmtAssign = new StmtAssign() {
                AnchorToken = idToken
            };
            stmtAssign.Add(Expr());
            Expect(TokenCategory.SEMICOLON);
            return stmtAssign;
        }

        public Node StmtIncr(Token idToken) {
            Expect(TokenCategory.PLUS_PLUS);
            var stmtIncr = new StmtIncr() {
                AnchorToken = idToken
            };
            Expect(TokenCategory.SEMICOLON);
            return stmtIncr;
        }

        public Node StmtDecr(Token idToken) {
            Expect(TokenCategory.NEG_NEG);
            var stmtDecr = new StmtDecr() {
                AnchorToken = idToken
            };
            Expect(TokenCategory.SEMICOLON);
            return stmtDecr;
        }

        public Node StmtFunCall(Token idToken) {
            Expect(TokenCategory.PARENTHESIS_OPEN);
            var stmtFunCall = new StmtFunCall(){
                AnchorToken = idToken
            };
            // if(CurrentToken != TokenCategory.PARENTHESIS_CLOSE) {
                stmtFunCall.Add(ExprList());
            // }
            Expect(TokenCategory.PARENTHESIS_CLOSE);
            return stmtFunCall;
        }

        public Node ExprList() {
            var exprList = new ExprList();
            if (CurrentToken != TokenCategory.PARENTHESIS_CLOSE) {
                exprList.Add(Expr());
                ExprListCont(ref exprList);
            }
            return exprList;
        }

        public Node ExprListCont(ref ExprList exprList) {
            while(CurrentToken == TokenCategory.COMMA) {
                Expect(TokenCategory.COMMA);
                exprList.Add(Expr());
            }
            return exprList;
        }

        public Node StmtIf() {
            var stmtIf = new StmtIf() {
                AnchorToken = Expect(TokenCategory.IF)
            };
            Expect(TokenCategory.PARENTHESIS_OPEN);
            Node ifExpr = Expr();
            Expect(TokenCategory.PARENTHESIS_CLOSE);
            Expect(TokenCategory.CURLY_OPEN);

            stmtIf.Add(ifExpr);
            stmtIf.Add(StmtList());
            Expect(TokenCategory.CURLY_CLOSE);

            while(CurrentToken == TokenCategory.ElSEIF) {
                stmtIf.Add(ElseIfList());
            }
            if(CurrentToken == TokenCategory.ELSE) {
                stmtIf.Add(Else());
            }

            return stmtIf;
        }

        public Node ElseIfList() {
            var stmtElseIf = new ElseIfList() {
                AnchorToken = Expect(TokenCategory.ElSEIF)
            };
            Expect(TokenCategory.PARENTHESIS_OPEN);
            stmtElseIf.Add(Expr());
            Expect(TokenCategory.PARENTHESIS_CLOSE);
            Expect(TokenCategory.CURLY_OPEN);
            stmtElseIf.Add(StmtList());
            Expect(TokenCategory.CURLY_CLOSE);
            return stmtElseIf;
        }

        public Node Else() {
            var stmtElse = new Else() {
                AnchorToken = Expect(TokenCategory.ELSE)
            };
            Expect(TokenCategory.CURLY_OPEN);
            stmtElse.Add(StmtList());
            Expect(TokenCategory.CURLY_CLOSE);
            return stmtElse;
        }

        public Node StmtWhile() {
            var stmtWhile = new StmtWhile() {
                AnchorToken = Expect(TokenCategory.WHILE)
            };
            Expect(TokenCategory.PARENTHESIS_OPEN);
            Node whileExpr = Expr();
            Expect(TokenCategory.PARENTHESIS_CLOSE);
            Expect(TokenCategory.CURLY_OPEN);
            stmtWhile.Add(whileExpr);
            stmtWhile.Add(StmtList());
            Expect(TokenCategory.CURLY_CLOSE);
            return stmtWhile;
        }

        public Node StmtBreak() {
            var stmtBreak = new StmtBreak() {
                AnchorToken = Expect(TokenCategory.BREAK)
            };
            Expect(TokenCategory.SEMICOLON);
            return stmtBreak;
        }
        
        public Node StmtReturn() {
            var stmtReturn = new StmtReturn() {
                AnchorToken = Expect(TokenCategory.RETURN)
            };
            
            stmtReturn.Add(Expr());
            Expect(TokenCategory.SEMICOLON);
            return stmtReturn;
        }

        public void StmtEmpty() {
            Expect(TokenCategory.SEMICOLON);   
        }

        public Node Expr() {
            return ExprOr();
        }

        public Node ExprOr() {
            var exprOr = ExprAnd();
            while(CurrentToken == TokenCategory.OR) {
                var n1 = new ExprOr(){
                    AnchorToken = Expect(TokenCategory.OR)
                };
                n1.Add(exprOr);
                n1.Add(ExprAnd());
                exprOr = n1;
            }

            return exprOr;
        }

        public Node ExprAnd() {
            var exprAnd = ExprComp();
            while(CurrentToken == TokenCategory.AND_COMP) {

                var n1 = new ExprAnd(){
                    AnchorToken = Expect(TokenCategory.AND_COMP)
                };
                n1.Add(exprAnd);
                n1.Add(ExprAnd());
                exprAnd = n1;
            }
            return exprAnd;
        }

        public Node ExprComp() {
            var exprRel = ExprRel();
            while(opComList.Contains(CurrentToken)) {
                var n1 = new ExprComp(){
                    AnchorToken = OpComp()
                };
                n1.Add(exprRel);
                n1.Add(ExprRel());
                exprRel = n1;
            }

            return exprRel;
        }

        public Token OpComp() {
            var token = tokenStream.Current;
            switch (CurrentToken) {

            case TokenCategory.DIST_COMPARISON:
                token = Expect(TokenCategory.DIST_COMPARISON);
                break;

            case TokenCategory.COMPARISON:
                token = Expect(TokenCategory.COMPARISON);
                break;
            default:
                throw new SyntaxError(opComList, tokenStream.Current);
            }
            return token;
        }

        public Node ExprRel() {
            var exprAdd = ExprAdd();
            while(opRelList.Contains(CurrentToken)) {
                var n1 = new ExprRel() {
                    AnchorToken = OpRel()
                };
                n1.Add(exprAdd);
                n1.Add(ExprAdd());
                exprAdd = n1;
            }
            return exprAdd;
        }

        public Token OpRel() {
            var token = tokenStream.Current;
            switch (CurrentToken) {
                case TokenCategory.LESS:
                    token = Expect(TokenCategory.LESS);
                    break;
                case TokenCategory.LESS_EQUAL:
                    token = Expect(TokenCategory.LESS_EQUAL);
                    break;
                case TokenCategory.GREATER:
                    token = Expect(TokenCategory.GREATER);
                    break;
                case TokenCategory.GREATER_EQUAL:
                    token = Expect(TokenCategory.GREATER_EQUAL);
                    break;
                default:
                    throw new SyntaxError(opRelList, tokenStream.Current);
                }
            return token;
        }

        public Node ExprAdd() {
            var exprMul = ExprMul();
            while(opAddList.Contains(CurrentToken)) {
                var n1 = new ExprAdd() {
                    AnchorToken = OpAdd()
                };

                n1.Add(exprMul);
                n1.Add(ExprMul());
                exprMul = n1;
            }
            return exprMul;
        }

        public Token OpAdd() {
            var token = tokenStream.Current;
            switch (CurrentToken) {

            case TokenCategory.PLUS:
                token = Expect(TokenCategory.PLUS);
                break;
            case TokenCategory.NEG:
                token = Expect(TokenCategory.NEG);
                break;
            default:
                throw new SyntaxError(opAddList, tokenStream.Current);
            }
            return token;
        }

        public Node ExprMul() {
            var exprUnary = ExprUnary();
            while(opMulList.Contains(CurrentToken)) {
                var n1 = new ExprMul() {
                    AnchorToken = OpMul()
                };


                n1.Add(exprUnary);
                n1.Add(ExprUnary());
                
                exprUnary = n1;
            }
            return exprUnary;
        }

        public Token OpMul() {
            var token = tokenStream.Current;
            switch (CurrentToken) {
            case TokenCategory.MUL:
                token = Expect(TokenCategory.MUL);
                break;
            case TokenCategory.DIV:
                token = Expect(TokenCategory.DIV);
                break;
            case TokenCategory.REMINDER:
                token = Expect(TokenCategory.REMINDER);
                break;
            default:
                throw new SyntaxError(opMulList, tokenStream.Current);
            }
            return token;
        }

        public Node ExprUnary() {
            while(opUnaryList.Contains(CurrentToken)) {
                Console.WriteLine(CurrentToken);
                var n1 = new ExprUnary() {
                    AnchorToken = OpUnary()
                };
                n1.Add(ExprUnary());
                return n1;
            }
            return ExprPrimary();
        }

        public Token OpUnary() {
            var token = tokenStream.Current;
            switch (CurrentToken) {

            case TokenCategory.PLUS:
                token = Expect(TokenCategory.PLUS);
                break;
            case TokenCategory.NEG:
                token = Expect(TokenCategory.NEG);
                break;
            case TokenCategory.NOT:
                token = Expect(TokenCategory.NOT);
                break;
            default:
                throw new SyntaxError(opUnaryList,tokenStream.Current);
            }
            return token;
        }

        public Node StmtFunCallExpr(Token idToken) {
            Expect(TokenCategory.PARENTHESIS_OPEN);
            var stmtFunCallExpr = new StmtFunCallExpr(){
                AnchorToken = idToken
            };
            stmtFunCallExpr.Add(ExprList());
            Expect(TokenCategory.PARENTHESIS_CLOSE);
            return stmtFunCallExpr;
        }

        public Node ExprPrimary() {
            var exprPrimary = new Node();
            switch (CurrentToken) {
                case TokenCategory.IDENTIFIER:
                    var identifier = Expect(TokenCategory.IDENTIFIER);
                    exprPrimary = new ExprPrimary();
                    exprPrimary.AnchorToken = identifier;
                    if(CurrentToken == TokenCategory.PARENTHESIS_OPEN) {
                        exprPrimary = StmtFunCallExpr(identifier);
                        exprPrimary.AnchorToken = identifier;
                    }
                    
                    break;
                case TokenCategory.BRACKET_OPEN:
                    exprPrimary = Array();
                    break;
                case TokenCategory.INT_LIT: 
                case TokenCategory.BOOL_LIT:
                case TokenCategory.CHAR_LIT:
                case TokenCategory.STRING_LIT:
                    exprPrimary = new ExprPrimary();
                    exprPrimary.AnchorToken = Lit();
                    break;
                case TokenCategory.PARENTHESIS_CLOSE:
                    break;
                default:
                    throw new SyntaxError(opLitList, tokenStream.Current);
            }
            return exprPrimary;
        }

       public Node Array() {
            var array = new Array();
            Expect(TokenCategory.BRACKET_OPEN);
            array.Add(ExprList());
            Expect(TokenCategory.BRACKET_CLOSE);
            return array;
        }
        
        public Token Lit() {
            var token = tokenStream.Current;
            switch (CurrentToken) {
            case TokenCategory.CHAR_LIT:
                token = Expect(TokenCategory.CHAR_LIT);
                break;
            case TokenCategory.BOOL_LIT:
                token = Expect(TokenCategory.BOOL_LIT);
                break;
            case TokenCategory.INT_LIT:
                token = Expect(TokenCategory.INT_LIT);
                break;
            case TokenCategory.STRING_LIT:
                token = Expect(TokenCategory.STRING_LIT);
                break;
            default:
                throw new SyntaxError(opLitList, tokenStream.Current);
            }
            return token;
        }
    }
}

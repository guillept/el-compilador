/*
  Wyvern compiler - Semantic Error.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/

using System;

namespace Wyvern {

    class SemanticError: Exception {

        public SemanticError(string message, Token token):
            base(String.Format(
                "Semantic Error: {0} \n" +
                "at row {1}, column {2}.",
                message,
                token.Row,
                token.Column)) {
        }
    }
}

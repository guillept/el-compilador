/*
  Wyvern compiler - Symbol Table.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace Wyvern {

    public class SymbolTable: IEnumerable<KeyValuePair<string, ArrayList>> {

        private String tableName = "";
        IDictionary<string, ArrayList> data = new SortedDictionary<string, ArrayList>();

        public SymbolTable(String tableName) {
            this.tableName = tableName;
        }
        
        public override string ToString() {
            var sb = new StringBuilder();
            sb.Append(tableName + "\n");
            var tab = "\t";
            if (tableName == "Global Variable Table" || tableName == "Functions Table") {
                sb.Append("====================\n");
                tab = "";
            }
    
            foreach (var entry in data) {
               if (entry.Value.Count > 0){
                    sb.Append(String.Format("{0}[{1}: {{{2}}}]\n", tab, entry.Key, Print(entry.Value)));
                } else {
                    sb.Append(String.Format("{0}[{1}]\n", tab, entry.Key));
                }
            }
            
            if (tableName == "Global Variable Table" || tableName == "Functions Table") {
                sb.Append("====================\n");
            }
    
            return sb.ToString();
        }

        public string Print(ArrayList values) {
            var sb = new StringBuilder();
            int last = values.Count - 1;
            int i = 0;
            foreach (var v in values) {
                var value = v == null ? '-' : v;    
                if ( i < last) 
                    sb.Append(String.Format("{0}, ", value));
                else
                    sb.Append(String.Format("{0}", value));

                i++;
            }
           return sb.ToString();

        }

        //-----------------------------------------------------------
        public ArrayList this[string key] {
            get {
                return data[key];
            }
            set {
                data[key] = value;
            }
        }

        //-----------------------------------------------------------
        public bool Contains(string key) {
            return data.ContainsKey(key);
        }

        //-----------------------------------------------------------
        public IEnumerator<KeyValuePair<string, ArrayList>> GetEnumerator() {
            return data.GetEnumerator();
        }

        //-----------------------------------------------------------
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            throw new NotImplementedException();
        }
    }
    
}

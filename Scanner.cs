/*
  Wyvern compiler - This class performs the lexical analysis, 
  (a.k.a. scanning).
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Wyvern {

    class Scanner {

        readonly string input;

        static readonly Regex regex = new Regex(


            @"
             (?<Multiline_Comment> [/][*](.|\n)*?[*][/])
           | (?<Comment>  [/][/].*)
           | (?<STRING_LIT> [""](([\\][nrt\\\""])|[^\n\r\t""])*[""]) 
           | (?<CHAR_LIT> [']([\\]u[a-fA-F\d]{6}|[\\][nrt\\\'\""]|[^\\^\x09^\n])['])
           | (?<DIST_COMPARISON> [!][=])
           | (?<COMPARISON> [=][=])
           | (?<ASSIGN> [=])
           | (?<LESS_EQUAL> [<][=])
           | (?<GREATER_EQUAL> [>][=])
           | (?<LESS> [<])
           | (?<GREATER> [>])
           | (?<PARENTHESIS_OPEN> [(])
           | (?<PARENTHESIS_CLOSE> [)])
           | (?<CURLY_OPEN> [{])
           | (?<CURLY_CLOSE> [}])
           | (?<BRACKET_OPEN> [\[])
           | (?<BRACKET_CLOSE> [\]])
           | (?<PLUS_PLUS> [+][+])
           | (?<NEG_NEG> [-][-])
           | (?<PLUS> [+])
           | (?<NEG> [-])
           | (?<MUL> [*])
           | (?<DIV> [/])
           | (?<REMINDER> [%])
           | (?<AND_COMP> (&&))
           | (?<AND> &)
           | (?<NOT> [!])
           | (?<OR> [|][|])
           | (?<SEMICOLON> ;)
           | (?<Newline> \n)
           | (?<Identifier> [a-zA-Z_][a-zA-Z0-9_]*)
           | (?<INT_LITERAL> [-]?[\d]+)
           | (?<WhiteSpace> \s )
           | (?<COMMA> ,)
           | (?<BAD_TOKEN> .)",
        
            RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled | RegexOptions.Multiline
            );

        static readonly IDictionary<string, TokenCategory> keywords =
            new Dictionary<string, TokenCategory>() {
                {"break", TokenCategory.BREAK},
                {"else", TokenCategory.ELSE},
                {"if", TokenCategory.IF},
                {"elseif", TokenCategory.ElSEIF},
                {"false", TokenCategory.BOOL_LIT},
                {"return", TokenCategory.RETURN},
                {"true", TokenCategory.BOOL_LIT},
                {"var", TokenCategory.VAR},
                {"while",TokenCategory.WHILE}
            };

        static readonly IDictionary<string, TokenCategory> nonKeywords =
            new Dictionary<string, TokenCategory>() {
                {"INT_LITERAL", TokenCategory.INT_LIT},
                {"CHAR_LIT", TokenCategory.CHAR_LIT},
                {"STRING_LIT", TokenCategory.STRING_LIT},
                {"IDENTIFIER", TokenCategory.IDENTIFIER},
                {"ASSIGN", TokenCategory.ASSIGN},
                {"DIST_COMPARISON", TokenCategory.DIST_COMPARISON},
                {"COMPARISON", TokenCategory.COMPARISON},
                {"LESS", TokenCategory.LESS},
                {"LESS_EQUAL", TokenCategory.LESS_EQUAL},
                {"GREATER", TokenCategory.GREATER},
                {"GREATER_EQUAL", TokenCategory.GREATER_EQUAL},
                {"PARENTHESIS_OPEN", TokenCategory.PARENTHESIS_OPEN},
                {"PARENTHESIS_CLOSE", TokenCategory.PARENTHESIS_CLOSE},
                {"CURLY_OPEN", TokenCategory.CURLY_OPEN},
                {"CURLY_CLOSE", TokenCategory.CURLY_CLOSE},
                {"BRACKET_OPEN", TokenCategory.BRACKET_OPEN},
                {"BRACKET_CLOSE", TokenCategory.BRACKET_CLOSE},
                {"PLUS", TokenCategory.PLUS},
                {"PLUS_PLUS", TokenCategory.PLUS_PLUS},
                {"NEG_NEG", TokenCategory.NEG_NEG},
                {"NEG", TokenCategory.NEG},
                {"MUL", TokenCategory.MUL},
                {"DIV", TokenCategory.DIV},
                {"REMINDER", TokenCategory.REMINDER},
                {"AND_COMP", TokenCategory.AND_COMP},
                {"AND", TokenCategory.AND},
                {"NOT", TokenCategory.NOT},
                {"OR", TokenCategory.OR},
                {"COMMA", TokenCategory.COMMA},
                {"SEMICOLON", TokenCategory.SEMICOLON}
            };

        public Scanner(string input) {
            this.input = input;
        }

        public IEnumerable<Token> Start() {

            var row = 1;
            var columnStart = 0;

            Func<Match, TokenCategory, Token> newTok = (m, tc) => new Token(m.Value, tc, row, m.Index - columnStart + 1);
            
            foreach (Match m in regex.Matches(input)) {
                if (m.Groups["Newline"].Success) {
                    row++;
                    columnStart = m.Index + m.Length;
                } else if (m.Groups["WhiteSpace"].Success || m.Groups["Comment"].Success) {
                    continue;
                } else if (m.Groups["Multiline_Comment"].Success){
                    var rowsToAdd = m.Value.Split('\n').Length - 1;
                    row += rowsToAdd;
                } else if (m.Groups["Identifier"].Success) {
                    if (keywords.ContainsKey(m.Value)) {
                        yield return newTok(m, keywords[m.Value]);
                    } else { 
                        yield return newTok(m, TokenCategory.IDENTIFIER);
                    }
                } else if (m.Groups["BAD_TOKEN"].Success) {
                    yield return newTok(m, TokenCategory.BAD_TOKEN);
                } else {
                    foreach (var name in nonKeywords.Keys) {
                        if (m.Groups[name].Success) {
                            yield return newTok(m, nonKeywords[name]);
                            break;
                        }
                    }
                }
            }

            yield return new Token(null, TokenCategory.EOF,  row, input.Length - columnStart + 1);
        }
    }
}

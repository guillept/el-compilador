/*
  Wyvern compiler - Main Error.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/

using System;

namespace Wyvern {

    class MainError: Exception {

        public MainError(string message):
            base(String.Format(message)) {
        }
    }
}

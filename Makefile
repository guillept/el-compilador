#
# Wyvern compiler - Project make file: 
# Copyright (C) 2019 
# Guillermo Perez Trueba A01377162
# Pablo Alejandro Sanchez Tadeo A01377515
# ITESM CEM
#  

all: wyvern.exe wyvernlib.dll

wyvern.exe: Driver.cs Scanner.cs Token.cs TokenCategory.cs Parser.cs SyntaxError.cs Node.cs Nodes.cs SemanticAnalyzer.cs SemanticError.cs SymbolTable.cs MainError.cs CILGenerator.cs wyvernlib.cs
	mcs -debug -out:wyvern.exe Driver.cs Scanner.cs Token.cs TokenCategory.cs Parser.cs SyntaxError.cs Node.cs Nodes.cs SemanticAnalyzer.cs SemanticError.cs SymbolTable.cs MainError.cs CILGenerator.cs wyvernlib.cs
			
wyvernlib.dll: wyvernlib.cs 
	mcs /t:library wyvernlib.cs
        
clean:
	rm *.exe wyvernlib.dll *.il

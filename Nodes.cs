/*
  Wyvern compiler - Program Nodes.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/
namespace Wyvern {
        class Program : Node {}
        class DefList : Node {}
        class VarDef : Node {}
        class VarList : Node {}
        class IdList : Node {}
        class FunDef : Node {}
        class FunDefList : Node {}
        class ParamList : Node {}
        class StmtList : Node {}
        class Stmt : Node {}
        class StmtAssign : Node {}
        class StmtIncr : Node {}
        class StmtDecr : Node {}
        class StmtFunCall : Node {}
        class StmtFunCallExpr : Node {}
        class FunCall : Node {}
        class ExprList : Node {}
        class ExprListCont : Node {}
        class StmtIf : Node {}
        class ElseIfList : Node {}
        class Else : Node {}
        class StmtWhile : Node {}
        class StmtBreak : Node {}
        class StmtReturn : Node {}
        class StmtEmpty : Node {}
        class Expr : Node {}
        class ExprOr : Node {}
        class ExprAnd : Node {}
        class ExprComp : Node {}
        class OpComp : Node {}
        class ExprRel : Node {}
        class OpRel : Node {}
        class ExprAdd : Node {}
        class OpAdd : Node {}
        class ExprMul : Node {}
        class OpMul : Node {}
        class ExprUnary : Node {}
        class OpUnary : Node {}
        class ExprPrimary : Node {}
        class Array : Node {}
        class Lit : Node {}
        class IntLit : Node {}
        class CharLit : Node {}
        class StringLit : Node {}
        class BoolLit : Node {}
        class Less : Node {}
        class LessEqual : Node {}
        class Greater : Node {}
        class GreaterEqual : Node {}

}
 
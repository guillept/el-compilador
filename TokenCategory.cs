/*
  Wyvern compiler - Token categories for the scanner.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/

namespace Wyvern {

    enum TokenCategory {
        INT_LIT,
        CHAR_LIT,
        STRING_LIT,
        BOOL_LIT,
        IDENTIFIER,
        BREAK,
        ELSE,
        ElSEIF,
        IF,
        RETURN,
        VAR,
        WHILE,
        ASSIGN,
        COMPARISON,
        DIST_COMPARISON,
        LESS,
        LESS_EQUAL,
        GREATER,
        GREATER_EQUAL,
        PARENTHESIS_OPEN,
        PARENTHESIS_CLOSE,
        PLUS,
        PLUS_PLUS,
        NEG_NEG,
        NEG,
        MUL,
        DIV,
        REMINDER,
        AND,
        AND_COMP,
        NOT,
        OR,
        PRINTI,
        PRINTC,
        PRINTS,
        PRINTLN,
        READI,
        READS,
        NEW,
        SIZE,
        ADD,
        GET,
        SET,
        SEMICOLON,
        COMMA,
        CURLY_OPEN,
        CURLY_CLOSE,
        BRACKET_OPEN,
        BRACKET_CLOSE,
        EOF,
        BAD_TOKEN
    }
}


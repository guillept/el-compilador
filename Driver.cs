/*
  Wyvern compiler - Program driver.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/

using System;
using System.IO;
using System.Text;

namespace Wyvern {

    public class Driver {

        const string VERSION = "4.0";
        
        void Run(string[] args) {

            Console.WriteLine("Wyvern compiler, version " + VERSION);
            Console.WriteLine("Copyright \u00A9 2019");
            Console.WriteLine("Guillermo Perez Trueba A01377162");
            Console.WriteLine("Pablo Alejandro Sanchez Tadeo A01377515");
            Console.WriteLine("ITESM CEM.");
            Console.WriteLine();

            if (args.Length != 2) {
                Console.Error.WriteLine("Please specify the name of the input file.");
                Environment.Exit(1);
            }
 
            try {            
                var inputPath = args[0];    
                var outputPath = args[1];            
                var input = File.ReadAllText(inputPath);
                var parser = new Parser(new Scanner(input).Start().GetEnumerator());
                var program = parser.Program();
                Console.WriteLine(program.ToStringTree());
                Console.WriteLine("Syntax OK.");

                var semantic = new SemanticAnalyzer();
                semantic.Visit((dynamic) program);
                
                Console.WriteLine("Semantics OK.");
                Console.WriteLine();

                var codeGenerator = new CILGenerator(semantic.GlobalVarTable, semantic.FunctionsSymbolTable);
                File.WriteAllText(
                    outputPath,
                    codeGenerator.Visit((dynamic) program));
                Console.WriteLine(
                    "Generated CIL code to '" + outputPath + "'.");
                Console.WriteLine();
            
            } catch (Exception e) {

                if (e is FileNotFoundException || e is SyntaxError  || e is SemanticError || e is MainError) {
                    Console.Error.WriteLine(e.Message);
                    Environment.Exit(1);
                }

                throw;
            }              
        }

        public static void Main(string[] args) {
            new Driver().Run(args);
        }
    }
}

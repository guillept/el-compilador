Wyvern compiler, version 0.5
===============================

This program is free software. You may redistribute it under the terms of
the GNU General Public License version 3 or later. See license.txt for 
details.    

Included in this release:

    * Lexical analysis
    * Syntactic analysis
    * AST construction
    * Semantic analysis
    * CIL code generation
    
To build, at the terminal type:

    make
   
To run, type:

    ./wyvernc <file_name>
    
Where <file_name> is the name of a Buttercup source file. You can try with
these files:

   * 001_ultimate.wyvern
   * 002_binary.wyvern
   * 003_palindrome.wyvern
   * 004_factorial.wyvern
   * 005_factorial.wyvern
   * 006_next_day.wyvern
   * 007_literals.wyvern
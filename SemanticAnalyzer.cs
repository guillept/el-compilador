/*
  Wyvern compiler - Semantic Analizer.
  Copyright (C) 2019 
  Guillermo Perez Trueba A01377162
  Pablo Alejandro Sanchez Tadeo A01377515
  ITESM CEM
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Wyvern {

    class SemanticAnalyzer {
        
        static Boolean isVarParam = true;
        static Boolean isSecondPass = false;
        static int isWhile = 0;
        static string funName = "";

    
        static readonly IDictionary<string, int> predefinedFunctions =
            new Dictionary<string, int>() {
                {"printi", 1},
                {"printc", 1},
                {"prints", 1},
                {"println", 0},
                {"readi", 0},
                {"reads", 0},
                {"new", 1},
                {"size", 1},
                {"add", 2},
                {"get", 2},
                {"set", 3},
            };
        
        public SymbolTable GlobalVarTable {
            get;
            private set;
        }

        public SymbolTable FunctionsSymbolTable {
            get;
            private set;
        }

        public SymbolTable LocalFunctionSymbolTable {
            get;
            private set;
        }
        
        //Aux functions
        
        public SemanticAnalyzer() {
            GlobalVarTable = new SymbolTable("Global Variable Table");
            FunctionsSymbolTable = new SymbolTable("Functions Table");
        }

        void VisitChildren(Node node) {
            foreach (var n in node) {
                Visit((dynamic) n);
            }
        }

        public void fillFuncTable() {
            FunctionsSymbolTable["printi"] = new ArrayList{1, true, null};
            FunctionsSymbolTable["printc"] = new ArrayList{1, true, null};
            FunctionsSymbolTable["prints"] = new ArrayList{1, true, null};
            FunctionsSymbolTable["println"] = new ArrayList{0, true, null};
            FunctionsSymbolTable["readi"] = new ArrayList{0, true, null};
            FunctionsSymbolTable["reads"] = new ArrayList{0, true, null};
            FunctionsSymbolTable["new"] = new ArrayList{1, true, null};
            FunctionsSymbolTable["size"] = new ArrayList{1, true, null};
            FunctionsSymbolTable["add"] = new ArrayList{2, true, null};
            FunctionsSymbolTable["get"] = new ArrayList{2, true, null};
            FunctionsSymbolTable["set"] = new ArrayList{3, true, null};
        }

        public void addPredifinedFunctions(string funName, int arity) {
            
            if (predefinedFunctions[funName] != arity) {
                throw new MainError(funName + " function expected " + predefinedFunctions[funName] + " parameters but " + arity + " were given");
            } else if (!FunctionsSymbolTable.Contains(funName)) {
                FunctionsSymbolTable[funName] = new ArrayList{arity, true, null};
            }

        }

        public Boolean isDeclared(string identifier, SymbolTable varTable) {
            if(varTable.Contains(identifier) || GlobalVarTable.Contains(identifier)) {
                return true;
            }
            return false;
        }

        public Boolean mainIsValid() {
            
            if(!FunctionsSymbolTable.Contains("main")) {
                throw new MainError("main function is not declared");
            }

            int arity = (int)FunctionsSymbolTable["main"][0];
            if(arity != 0) {
                throw new MainError("main function expected 0 parameters but " + arity + " were given");
            }

            return true;
        }
        
        //Functional funcs

        public void Visit(Program node) {
            fillFuncTable();
            VisitChildren(node);
            isSecondPass = true;
            Visit((dynamic) node[1]);
            mainIsValid();
        }

        public void Visit(VarList node) {
            isVarParam = false;
            VisitChildren(node);
        }

        public void Visit(VarDef node) {
            VisitChildren(node);
        }

        public void Visit(IdList node) {
    
            var variableName = node.AnchorToken.Lexeme;

            if(!isSecondPass) {
                if (GlobalVarTable.Contains(variableName)) {
                    throw new SemanticError("Duplicated variable: " + variableName, node.AnchorToken);
                } else {
                    GlobalVarTable[variableName] = new ArrayList();              
                }
            } else {
                if (((SymbolTable)FunctionsSymbolTable[funName][2]).Contains(variableName)) {
                    throw new SemanticError("Duplicated variable in " + funName + ": " + variableName, node.AnchorToken);
                } else {
                    ((SymbolTable)FunctionsSymbolTable[funName][2])[variableName] = new ArrayList{isVarParam};
                }
            }
            
        }

        public void Visit(FunDefList node) {
            
            VisitChildren(node);            
        }

        public void Visit(FunDef node) {
            var variableName = node.AnchorToken.Lexeme;
            var arity = node[0].children.Count;

            if(!isSecondPass) {
                if (FunctionsSymbolTable.Contains(variableName) || predefinedFunctions.ContainsKey(variableName) ) {
                    throw new SemanticError("Duplicated function: " + variableName, node.AnchorToken);
                } else {
                    SymbolTable funSymTable = new SymbolTable(variableName+" Table");
                    FunctionsSymbolTable[variableName] = new ArrayList{arity, false, funSymTable};
                }
            } else {
                funName = variableName;
                VisitChildren(node);
            }
        }

        public void Visit(ParamList node) {
            isVarParam = true;
            VisitChildren(node);
        }

        public void Visit(StmtList node) {
            VisitChildren(node);
        }

        public void Visit(StmtAssign node) {
            if(node.AnchorToken.Category == TokenCategory.IDENTIFIER) {
                string idName = node.AnchorToken.Lexeme;
                if(!isDeclared(idName, (SymbolTable)FunctionsSymbolTable[funName][2])){
                    throw new SemanticError(idName + " is not declared", node.AnchorToken);
                } else {
                    VisitChildren(node);
                }
            }
        }

        public void Visit(StmtIncr node) {
            if(node.AnchorToken.Category == TokenCategory.IDENTIFIER) {
                string idName = node.AnchorToken.Lexeme;
                if(!isDeclared(idName, (SymbolTable)FunctionsSymbolTable[funName][2])){
                    throw new SemanticError(idName + " is not declared", node.AnchorToken);
                } else {
                    VisitChildren(node);
                }
            }
        }

        public void Visit(StmtDecr node) {
            if(node.AnchorToken.Category == TokenCategory.IDENTIFIER) {
                string idName = node.AnchorToken.Lexeme;
                if(!isDeclared(idName, (SymbolTable)FunctionsSymbolTable[funName][2])){
                    throw new SemanticError(idName + " is not declared", node.AnchorToken);
                } else {
                    VisitChildren(node);
                }
            }
        }

        public void Visit(StmtFunCall node) {
            string funName = node.AnchorToken.Lexeme;
            var arity = node[0].children.Count;
            
            var predifined = predefinedFunctions.ContainsKey(funName);
            if (predifined) {
                addPredifinedFunctions(funName, arity);
            }

            if (!FunctionsSymbolTable.Contains(funName)) {
                throw new SemanticError(funName + " function is not declared", node.AnchorToken);
            }
            if ((int)FunctionsSymbolTable[funName][0] != arity) {
                throw new SemanticError(funName + " expected " + FunctionsSymbolTable[funName][0] + " parameters but " + arity  + " were given", node.AnchorToken);
            }
            VisitChildren(node);
        }

        public void Visit(StmtFunCallExpr node) {
            string funName = node.AnchorToken.Lexeme;
            var arity = node[0].children.Count;
            
            var predifined = predefinedFunctions.ContainsKey(funName);
            if (predifined) {
                addPredifinedFunctions(funName, arity);
            }

            if (!FunctionsSymbolTable.Contains(funName)) {
                throw new SemanticError(funName + " function is not declared", node.AnchorToken);
            }
            if ((int)FunctionsSymbolTable[funName][0] != arity) {
                throw new SemanticError(funName + " expected " + FunctionsSymbolTable[funName][0] + " parameters but " + arity  + " were given", node.AnchorToken);
            }
            VisitChildren(node);
        }

        public void Visit(StmtIf node) {
            VisitChildren(node);
        }

        public void Visit(ElseIfList node) {
            VisitChildren(node);
        }

        public void Visit(Else node) {
            VisitChildren(node);
        }

        public void Visit(StmtWhile node) {
            isWhile += 1;
            VisitChildren(node);
            isWhile -= 1;
        }

        public void Visit(StmtBreak node) {
            if (isWhile == 0) {
                throw new SemanticError("Break is not inside a while function", node.AnchorToken);
            }
        }

        public void Visit(StmtReturn node) {}

        public void Visit(StmtEmpty node) {}

        public void Visit(ExprList node) {
            VisitChildren(node);
        }

        public void Visit(ExprOr node) {
            VisitChildren(node);
        }

        public void Visit(ExprAnd node) {
            VisitChildren(node);
        }

        public void Visit(ExprComp node) {
            VisitChildren(node);
        }

        public void Visit(OpComp node) {
            VisitChildren(node);
        }

        public void Visit(ExprRel node) {
            VisitChildren(node);
        }

        public void Visit(OpRel node) {
            VisitChildren(node);
        }

        public void Visit(ExprAdd node) {
            VisitChildren(node);
        }

        public void Visit(OpAdd node) {
            VisitChildren(node);
        }

        public void Visit(ExprMul node) {
            VisitChildren(node);
        }

        public void Visit(OpMul node) {
            VisitChildren(node);
        }

        public void Visit(ExprUnary node) {
            VisitChildren(node);
        }

        public void Visit(OpUnary node) {
            VisitChildren(node);
        }

        public void Visit(Array node) {
            VisitChildren(node);
        }

        public void Visit(ExprPrimary node) {
            if(node.AnchorToken.Category == TokenCategory.IDENTIFIER) {
                string idName = node.AnchorToken.Lexeme;
                if(!isDeclared(idName,(SymbolTable)FunctionsSymbolTable[funName][2])) {
                    throw new SemanticError(idName + " is not declared", node.AnchorToken); 
                } else {
                    VisitChildren(node);
                }
            }

            if (node.AnchorToken.Category == TokenCategory.INT_LIT) {
                string idName = node.AnchorToken.Lexeme;
                try {
                    Convert.ToInt32(idName);
                } catch (OverflowException) {
                    throw new SemanticError("Integer literal too large: " + idName, node.AnchorToken);
                }
            }
        }

    }
}